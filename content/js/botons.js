const animatedClassName = "animated";
const span_elements = [];

document.querySelectorAll(".HOVER").forEach((element, index) => {
	let addAnimation = false;

	if (element.classList[1] == "FLASH") {
		element.addEventListener("animationend", e => {
			element.classList.remove(animatedClassName);
		});
		addAnimation = true;
	}

	if (!span_elements[index])
		span_elements[index] = element.querySelector("span");

	element.addEventListener("mouseover", e => {
		span_elements[index].style.left = e.clientX - element.getBoundingClientRect().left + "px";
		span_elements[index].style.top = e.clientY - element.getBoundingClientRect().top + "px";

		if (addAnimation) element.classList.add(animatedClassName);
	});

	element.addEventListener("mouseout", e => {
		span_elements[index].style.left = e.clientX - element.getBoundingClientRect().left + "px";
		span_elements[index].style.top = e.clientY - element.getBoundingClientRect().top + "px";
	});
});