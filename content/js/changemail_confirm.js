$(document).ready(() => {
    $('#confirm-button').click((e) => {
        e.preventDefault();
        $('#confirm-button').prop('disabled', true);
        $.post('/api/cabinet/mail/confirm', {
            key: $('#key-input').val(),
        }, (data) => {
            if (!data.success) {
                $('#confirm-button').prop('disabled', false);
                displayError(data.error);
                return;
            }

            note({
                content: getTranslation(results, 'changemail-confirm'),
                type: 'info',
                time: 30
            });
        });
    });

    function checkWholeValidity() {
        let enabled = $('#key-input').val() !== "";

        $('#confirm-button').prop('disabled', !enabled);
    }

    $('#key-input').on('keyup keypress change input', () => {
        checkWholeValidity();
    });

    checkWholeValidity();

    if ($('#key-input').val() !== "") {
        $('#confirm-button').trigger('click');
    }
});
