let skinViewer;

function UrlExists(url, callback) {
    fetch(url, { method: 'head' })
    .then(function(status) {
      callback(status.ok)
    });
  }

function reloadSkin() {
    const url = `https://storage.c7x.dev/profile-images/skin/${user.uuid}`;
    UrlExists(url, function(exists){
        if(exists){
            skinViewer.loadSkin(url, window.skin_model ? 'default' : 'slim'); 
        }else{
            var skinName = window.skin_model ? 'steve_skin.png' : 'alex_skin.png';
            skinViewer.loadSkin(`https://storage.c7x.dev/profile-images/skin/${skinName}`, window.skin_model ? 'default' : 'slim');
        }
    });
}

function reloadCape() {
    const url = `https://storage.c7x.dev/profile-images/cape/${user.uuid}`;
    UrlExists(url, function(exists){
        if(exists){
            skinViewer.loadCape(url);
        }else{
            skinViewer.loadCape(null);
        }
    });

}

async function recreateSkinView() {
    if(!skinViewer){
        skinViewer = new skinview3d.FXAASkinViewer({
            canvas: document.getElementById("skin_container"),
            alpha: true
        });
        orbitControl = skinview3d.createOrbitControls(skinViewer);
    
        skinViewer.width = 650;
        skinViewer.height = 800;
        skinViewer.animations.speed = 1.0;
    }
    
    primaryAnimation = skinViewer.animations.add(skinview3d.WalkingAnimation);

    reloadSkin();
    reloadCape();
}

function skinButton(skinType) {
    if ('alex' === skinType) {
        $('#alex-skin-button').attr('disabled', 'disabled');
        $('#steve-skin-button').removeAttr('disabled');
    } else {
        $('#steve-skin-button').attr('disabled', 'disabled');
        $('#alex-skin-button').removeAttr('disabled');
    }
}

function handlerForSkinButton(skinType) {
    return () => {
        var ser = skinType === 'steve' ? true : false;
        if (window.skin_model !== ser) {
            window.skin_model = ser;
            $.post('/api/cabinet/skin_type', {
                skin_type: skinType,
            }, (data) => {
                if (!data.success) {
                    displayError(data.error);
                    return;
                }
                recreateSkinView();
                skinButton(skinType);
            });
        }
    };
}

function loadOtp() {
    $.post('/api/cabinet/otp/stage_1', {}, (data) => {
        if (!data.success) {
            displayError(data.error);
            return;
        }
        $('#otp-qr-code-image').attr('src', data.result.image_url);
    });
}

function componentToHex(c) {
  var hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
  return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

$(document).ready(() => {
    (async () => {
        skinButton(window.skin_model ? 'steve' : 'alex');
        await recreateSkinView();
    })();

    const frames = [
        'change-color',
        'change-username',
        'change-email',
        'change-password',
        'change-session',
        'change-2fa',
        'buy-hd',
        'pay'
    ];

    for (let i = 0; i < frames.length; i++) {
        const frameId = frames[i];
        $(`#${frameId}-button`).click(() => {
            $('#modal-frame').removeClass('re-frame2');
            $('#modal-frame').addClass('re-frame1');
            $('body').addClass('stop-scrolling')
            $(`#${frameId}-modal`).removeClass('vanish');
            $(`#${frameId}-modal`).addClass('display');
        });

        const closeHandler = () => {
            $('#modal-frame').addClass('re-frame2');
            $('#modal-frame').removeClass('re-frame1');
            $('body').removeClass('stop-scrolling')
            $(`#${frameId}-modal`).addClass('vanish');
            $(`#${frameId}-modal`).removeClass('display');
        };

        $(`#close-${frameId}-modal`).click(closeHandler);
        $(`#close-${frameId}-modal-button`).click(closeHandler);
    }

    $('#hd-sub-btn').click(() => {
        var hd_sub_value = $('#sub-hd-fld').val();
        if (hd_sub_value.length === 0) {
            displayError('Выберите срок подписки!');
            return;
        }
        $('#modal-frame').addClass('re-frame2');
        $('#modal-frame').removeClass('re-frame1');
        $('body').removeClass('stop-scrolling')
        $(`#buy-hd-modal`).addClass('vanish');
        $(`#buy-hd-modal`).removeClass('display');
        $.post('/api/cabinet/hd', {
            hd_time: hd_sub_value
        }, (data) => {
            if (!data.success) {
                displayError(data.error);
                return;
            }
            note({
                content: getTranslation(results, 'buy-hd'),
                type: 'info',
                time: 30
            });
        });
    });

    $('#otp-submit').click(() => {
        $('#otp-submit').prop('disabled', true);
        $.post('/api/cabinet/otp/stage_2', {
            otp_code: $('#otp-stage2-code').val()
        }, (data) => {
            $('#otp-submit').prop('disabled', false);
            if (!data.success) {
                displayError(data.error);
                return;
            }
            $('#modal-frame').addClass('re-frame2');
            $('#modal-frame').removeClass('re-frame1');
            $('body').removeClass('stop-scrolling')
            $(`#change-2fa-modal`).addClass('vanish');
            $(`#change-color-modal`).removeClass('display');
            note({
                content: getTranslation(results, '2fa-active'),
                type: 'info',
                time: 30
            });
        });
    });

    $('#confirm-change-color').click(() => {
        $('#modal-frame').addClass('re-frame2');
        $('#modal-frame').removeClass('re-frame1');
        $('body').removeClass('stop-scrolling')
        $(`#change-color-modal`).addClass('vanish');
        $(`#change-color-modal`).removeClass('display');
        var splitted = $('#color-picker-example-text').css('color').split(',');
        var color = rgbToHex(+splitted[0].substring(4), +splitted[1].substring(1), +splitted[2].substring(1).replace(')',''));
        $.post('/api/cabinet/name_color', {
            color: color,
        }, (data) => {
            if (!data.success) {
                displayError(data.error);
                return;
            }
            note({
                content: getTranslation(results, 'change-name-color'),
                type: 'info',
                time: 30
            });
        })
    });

    $('#confirm-nickname-change-btn').click(() => {
        $('#modal-frame').addClass('re-frame2');
        $('#modal-frame').removeClass('re-frame1');
        $('body').removeClass('stop-scrolling')
        $(`#change-username-modal`).addClass('vanish');
        $(`#change-username-modal`).removeClass('display');
        $.post('/api/cabinet/username', {
            password: $('#change-username-current-password-field').val(),
            new_username: $('#change-username-new-username-input').val()
        }, (data) => {
            if (!data.success) {
                displayError(data.error);
                return;
            }
            note({
                content: getTranslation(results, 'change-nickname'),
                type: 'info',
                time: 30
            });
        });
    });
    $('#change-email-btn').click(() => {
        $('#modal-frame').addClass('re-frame2');
        $('#modal-frame').removeClass('re-frame1');
        $('body').removeClass('stop-scrolling')
        $(`#change-email-modal`).addClass('vanish');
        $(`#change-email-modal`).removeClass('display');
        $.post('/api/cabinet/mail', {
            password: $('#change-email-current-password-field').val(),
            new_email: $('#change-email-new-email-input').val()
        }, (data) => {
            if (!data.success) {
                displayError(data.error);
                return;
            }
            note({
                content: getTranslation(results, 'change-email'),
                type: 'info',
                time: 30
            });
        });
    });
    $('#change-password-button-submit').click(() => {
        $('#modal-frame').addClass('re-frame2');
        $('#modal-frame').removeClass('re-frame1');
        $('body').removeClass('stop-scrolling')
        $(`#change-password-modal`).addClass('vanish');
        $(`#change-password-modal`).removeClass('display');
        var newPassword = $('#change-password-password-input').val();
        if (newPassword !== $('#change-password-repeat-password-input').val()) {
            displayError('Пароли не совпадают!');
            return;
        }
        $.post('/api/cabinet/password', {
            old_password: $('#change-password-current-password-input').val(),
            new_password: newPassword
        }, (data) => {
            if (!data.success) {
                displayError(data.error);
                return;
            }
            note({
                content: getTranslation(results, 'change-password'),
                type: 'info',
                time: 30
            });
        });
    });

    $('#change-2fa-button').click(() => {
        loadOtp();
    });

    $('#logout-button').click(() => {
        $('#logout-button').prop('disabled', true);
        $.post('/api/auth/logout', {}, () => {
            document.location.href = '/';
        });
    })

    $('#change-skin-button').click(() => {
        let skinFileInput = $('<input>');
        skinFileInput.attr('type', 'file');
        skinFileInput.attr('accept', 'image/png');
        skinFileInput.attr('name', 'skin');

        let skinFileForm = $('<form>');
        skinFileForm.append(skinFileInput);

        skinFileInput.change(() => {
            let file = skinFileInput[0].files[0];

            if (file.size > 500 * 1024) {
                displayError('file-is-too-big');
                return;
            }

            $('#change-skin-button').attr('disabled', true);
            $.ajax({
                url: '/api/cabinet/skin',
                type: 'POST',
                data: new FormData(skinFileForm[0]),
                processData: false,
                contentType: false
            }).done((data) => {
                $('#change-skin-button').prop('disabled', false);

                if (!data.success) {
                    displayError(data.error);
                    return;
                }

                recreateSkinView();
                note({
                    content: getTranslation(results, 'change-skin'),
                    type: 'info',
                    time: 30
                });
            });
        });
        skinFileInput.trigger('click');
    });

    $('#change-cape-button').click(() => {
        let capeFileInput = $('<input>');
        capeFileInput.attr('type', 'file');
        capeFileInput.attr('accept', 'image/png');
        capeFileInput.attr('name', 'cape');

        let capeFileForm = $('<form>');
        capeFileForm.append(capeFileInput);

        capeFileInput.change(() => {
            let file = capeFileInput[0].files[0];

            if (file.size > 500 * 1024) {
                displayError('file-is-too-big');
                return;
            }

            $('#change-cape-button').attr('disabled', true);
            $.ajax({
                url: '/api/cabinet/cape',
                type: 'POST',
                data: new FormData(capeFileForm[0]),
                processData: false,
                contentType: false
            }).done((data) => {
                $('#change-cape-button').prop('disabled', false);

                if (!data.success) {
                    displayError(data.error);
                    return;
                }

                recreateSkinView();
                note({
                    content: getTranslation(results, 'change-cape'),
                    type: 'info',
                    time: 30
                });
            });
        });
        capeFileInput.trigger('click');
    });

    $('#color-picker-input').on('input change', () => {
        $('#color-picker-example-text').css('color', $('#color-picker-input').val());
    })

    $('#steve-skin-button').click(handlerForSkinButton('steve'));
    $('#alex-skin-button').click(handlerForSkinButton('alex'));

    let usernameRegex = /(?:^([a-zA-Z0-9_]{4,16})$|^([а-яА-Я0-9_]{4,16})$)/;
    let passwordRegex = /^(?=.*[A-ZА-Я])(?=.*[0-9])(?=.*[a-zа-я]).{8,}$/;
    let emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    let lastUsernameValue = undefined;
    let lastEmailValue = undefined;

    function inputNone(inputId) {
        $(`#${inputId}-input`).removeClass('invalid-input');
        $(`#${inputId}-input`).removeClass('valid-input');
        $(`#${inputId}-error-block`).removeClass('flash-error-visible');
    }

    function inputSuccess(inputId) {
        $(`#${inputId}-input`).removeClass('invalid-input');
        $(`#${inputId}-input`).removeClass('valid-input');
        $(`#${inputId}-input`).addClass('valid-input');
        $(`#${inputId}-error-block`).removeClass('flash-error-visible');
    }

    function inputError(inputId, error) {
        $(`#${inputId}-input`).removeClass('invalid-input');
        $(`#${inputId}-input`).removeClass('valid-input');
        $(`#${inputId}-input`).addClass('invalid-input');

        if (error === undefined) {
            return;
        }

        $(`#${inputId}-error`).text(getTranslation(errors, error));
        $(`#${inputId}-error-block`).removeClass('flash-error-visible');
        $(`#${inputId}-error-block`).addClass('flash-error-visible');
    }

    function checkWholeValidityName() {
        let enabled = $('#change-username-new-username-input').hasClass('valid-input');

        $('#confirm-nickname-change-btn').prop('disabled', !enabled);
    }

    function checkWholeValidityEmail() {
        let enabled = $('#change-email-new-email-input').hasClass('valid-input');

        $('#change-email-btn').prop('disabled', !enabled);
    }

    function checkWholeValidityPassword() {
        let enabled = $('#change-password-password-input').hasClass('valid-input') &&
            $('#change-password-repeat-password-input').hasClass('valid-input');

        $('#change-password-button-submit').prop('disabled', !enabled);
    }

    $('#change-username-new-username-input').on('keyup keypress change input', (event) => {
        let currentValue = $(event.target).val();

        if (currentValue === lastUsernameValue) {
            return;
        }
        lastUsernameValue = currentValue;

        if (currentValue === '') {
            inputNone('change-username-new-username');
            checkWholeValidityName();
            return;
        }

        if (!usernameRegex.test(currentValue)) {
            inputError('change-username-new-username', 'username-format-incorrect');
            checkWholeValidityName();
            return;
        }

        $.post('/api/auth/register/username_available', {
            username: currentValue
        }, (data) => {
            if (currentValue !== $(event.target).val()) {
                return;
            }

            if (!data.success) {
                displayError(data.error);
                return;
            }

            if (!data.result.available) {
                inputError('change-username-new-username', 'username-unavailable');
                checkWholeValidityName();
                return;
            }

            inputSuccess('change-username-new-username');
            checkWholeValidityName();
        });
    });

    $('#change-email-new-email-input').on('keyup keypress change input', (event) => {
        let currentValue = $(event.target).val();

        if (currentValue === lastEmailValue) {
            return;
        }
        lastEmailValue = currentValue;

        if (currentValue === '') {
            inputNone('change-email-new-email');
            checkWholeValidityEmail();
            return;
        }

        if (!emailRegex.test(currentValue)) {
            inputError('change-email-new-email', 'email-format-incorrect');
            checkWholeValidityEmail();
            return;
        }

        $.post('/api/auth/register/email_available', {
            email: currentValue
        }, (data) => {
            if (currentValue !== $(event.target).val()) {
                return;
            }

            if (!data.success) {
                displayError(data.error);
                return;
            }

            if (!data.result.available) {
                inputError('change-email-new-email', 'email-unavailable');
                checkWholeValidityEmail();
                return;
            }

            inputSuccess('change-email-new-email');
            checkWholeValidityEmail();
        });
    });

    $('#change-password-password-input').on('keyup keypress change input', (event) => {
        let currentValue = $(event.target).val();

        if (currentValue === '') {
            inputNone('change-password-password');
            checkWholeValidityPassword();
            return;
        }

        if (!passwordRegex.test(currentValue)) {
            inputError('change-password-password', 'password-format-incorrect');
            checkWholeValidityPassword();
            return;
        }

        inputSuccess('change-password-password');
        $('#change-password-repeat-password-input').trigger('change');
        checkWholeValidityPassword();
    });

    $('#change-password-repeat-password-input').on('keyup keypress change input', (event) => {
        let currentValue = $(event.target).val();

        if (currentValue === '') {
            inputNone('change-password-repeat-password');
            checkWholeValidityPassword();
            return;
        }

        if (!passwordRegex.test(currentValue)) {
            inputError('change-password-repeat-password', 'password-format-incorrect');
            checkWholeValidityPassword();
            return;
        }

        if (currentValue !== $('#change-password-password-input').val()) {
            inputError('change-password-repeat-password', 'repeat-password-incorrect');
            checkWholeValidityPassword();
            return;
        }

        inputSuccess('change-password-repeat-password');
        checkWholeValidityPassword();
    });
});