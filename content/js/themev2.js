document.addEventListener("DOMContentLoaded", function(event) {
    if(localStorage.getItem('theme') === null){
        isDarkMode() ? localStorage.setItem('theme', 'dark') : localStorage.setItem('theme', 'light');
        document.cookie = "theme="+ localStorage.getItem('theme') +"; max-age=93312000; path=/; domain=test.crimap.ru";
        document.location.reload(true);
    }
});

function iwanntchangetheme() {

    if (localStorage.getItem('theme') === "dark") {
        localStorage.setItem('theme', 'light');
    } else if(localStorage.getItem('theme') === "light") {
        localStorage.setItem('theme', 'dark');
    } else {
        localStorage.setItem('theme', 'light');
    }

    document.cookie = "theme="+ localStorage.getItem('theme') +"; max-age=93312000; path=/; domain=test.crimap.ru";

    document.location.reload(true);
}

function isDarkMode(){
    return window.matchMedia('(prefers-color-scheme: dark)').matches;
}

function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function delCookie(name) {
    document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}