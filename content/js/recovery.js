$(document).ready(() => {
    $('#recovery-button').click((e) => {
        e.preventDefault();
        $('#recovery-button').prop('disabled', true);
        grecaptcha.ready(function () {
            grecaptcha.execute('6Ld6jWcaAAAAAArgu8eF5Dlltmc4IQkoXW7m6dJD', {
                action: 'password_recovery'
            }).then(function (token) {
                $.post('/api/auth/recovery', {
                    email: $('#email-input').val(),
                    captcha: token
                }, (data) => {
                    $('#recovery-button').prop('disabled', false);

                    if (!data.success) {
                        displayError(data.error);
                        return;
                    }

                    note({
                        content: getTranslation(results, 'recovery'),
                        type: 'info',
                        time: 30
                    });
                });
            });
        });
    });

    let emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

    function inputNone(inputId) {
        $(`#${inputId}-input`).removeClass('invalid-input');
        $(`#${inputId}-input`).removeClass('valid-input');
        $(`#${inputId}-error-block`).removeClass('flash-error-visible');
    }

    function inputSuccess(inputId) {
        $(`#${inputId}-input`).removeClass('invalid-input');
        $(`#${inputId}-input`).removeClass('valid-input');
        $(`#${inputId}-input`).addClass('valid-input');
        $(`#${inputId}-error-block`).removeClass('flash-error-visible');
    }

    function inputError(inputId, error) {
        $(`#${inputId}-input`).removeClass('invalid-input');
        $(`#${inputId}-input`).removeClass('valid-input');
        $(`#${inputId}-input`).addClass('invalid-input');

        if (error === undefined) {
            return;
        }

        $(`#${inputId}-error`).text(getTranslation(errors, error));
        $(`#${inputId}-error-block`).removeClass('flash-error-visible');
        $(`#${inputId}-error-block`).addClass('flash-error-visible');
    }

    function checkWholeValidity() {
        let enabled = $('#email-input').hasClass('valid-input');

        $('#register-button').prop('disabled', !enabled);
    }

    $('#email-input').on('keyup keypress change input', (event) => {
        let currentValue = $(event.target).val();

        if (currentValue === '') {
            inputNone('email');
            checkWholeValidity();
            return;
        }

        if (!emailRegex.test(currentValue)) {
            inputError('email', 'email-format-incorrect');
            checkWholeValidity();
            return;
        }

        inputSuccess('email');
        checkWholeValidity();
    });
});
