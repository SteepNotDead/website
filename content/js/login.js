$(document).ready(() => {
    var otpfrst = false;

    function openOtp() {
            $('#modal-frame').removeClass('re-frame2');
            $('#modal-frame').addClass('re-frame1');
            $('body').addClass('stop-scrolling')
            $(`#otp-modal`).removeClass('vanish');
            $(`#otp-modal`).addClass('display');
    }
    const frames = [
        'otp'
    ];

    for (let i = 0; i < frames.length; i++) {
        const frameId = frames[i];

        const closeHandler = () => {
            $('#modal-frame').addClass('re-frame2');
            $('#modal-frame').removeClass('re-frame1');
            $('body').removeClass('stop-scrolling')
            $(`#${frameId}-modal`).addClass('vanish');
            $(`#${frameId}-modal`).removeClass('display');
        };

        $(`#close-${frameId}-modal`).click(closeHandler);
        $(`#close-${frameId}-modal-button`).click(closeHandler);
    }
    function sendRequest() {
        grecaptcha.ready(function () {
            grecaptcha.execute('6Ld6jWcaAAAAAArgu8eF5Dlltmc4IQkoXW7m6dJD', {
                action: 'login'
            }).then(function (token) {
                $.post('/api/auth/login', {
                    username: $('#username-input').val(),
                    password: $('#password-input').val(),
                    otp: $('#otp-input-field').val(),
                    captcha: token
                }, (data) => {
                    if (!data.success) {
                        $('#login-button').prop('disabled', false);
                        $('#confirm-otp-btn').prop('disabled', false);
                        if (data.error === 'otp-required' || data.error === 'otp-incorrect') {
                            if (otpfrst) {
                                displayError('Неверный код!');
                            }
                            otpfrst = true;
                            openOtp();
                            return;
                        }

                        displayError(data.error);
                        return;
                    }

                    location.replace('/cabinet');
                });
            });
        });
    }
    $('#login-button').click((e) => {
        e.preventDefault();
        $('#login-button').prop('disabled', true);
        sendRequest();
    });
    $('#confirm-otp-btn').click((e) => {
        $('#confirm-otp-btn').prop('disabled', true);
        sendRequest();
    });

    let otpRegex = /^([0-9]{6})$/;

    function inputNone(inputId) {
        $(`#${inputId}-input`).removeClass('invalid-input');
        $(`#${inputId}-input`).removeClass('valid-input');
        $(`#${inputId}-error-block`).removeClass('flash-error-visible');
    }

    function inputSuccess(inputId) {
        $(`#${inputId}-input`).removeClass('invalid-input');
        $(`#${inputId}-input`).removeClass('valid-input');
        $(`#${inputId}-input`).addClass('valid-input');
        $(`#${inputId}-error-block`).removeClass('flash-error-visible');
    }

    function inputError(inputId, error) {
        $(`#${inputId}-input`).removeClass('invalid-input');
        $(`#${inputId}-input`).removeClass('valid-input');
        $(`#${inputId}-input`).addClass('invalid-input');

        if (error === undefined) {
            return;
        }

        $(`#${inputId}-error`).text(getTranslation(errors, error));
        $(`#${inputId}-error-block`).removeClass('flash-error-visible');
        $(`#${inputId}-error-block`).addClass('flash-error-visible');
    }

    function checkWholeValidity() {
        let enabled = $('#username-input').val() !== "" &&
            ($('#otp-input').hasClass('valid-input') || $('#otp-input').is(':hidden')) &&
            $('#password-input').val() !== "";

        $('#login-button').prop('disabled', !enabled);
    }

    $('#username-input').on('keyup keypress change input', () => {
        checkWholeValidity();
    });

    $('#password-input').on('keyup keypress change input', () => {
        checkWholeValidity();
    });

    $('#otp-input').on('keyup keypress change input', (event) => {
        let currentValue = $(event.target).val();

        if (currentValue === '') {
            inputNone('otp');
            checkWholeValidity();
            return;
        }

        if (!otpRegex.test(currentValue)) {
            inputError('otp');
            checkWholeValidity();
            return;
        }

        inputSuccess('otp');
        checkWholeValidity();
    });
});
