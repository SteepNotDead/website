$(document).ready(() => {
    $('#confirm-button').click((e) => {
        e.preventDefault();
        $('#confirm-button').prop('disabled', true);
        $.post('/api/auth/recovery/confirm', {
            key: $('#key-input').val(),
            new_password: $('#password-input').val()
        }, (data) => {
            if (!data.success) {
                $('#confirm-button').prop('disabled', false);
                displayError(data.error);
                return;
            }

            note({
                content: getTranslation(results, 'recovery-confirm'),
                type: 'info',
                time: 30
            });
        });
    });

    let passwordRegex = /^(?=.*[A-ZА-Я])(?=.*[0-9])(?=.*[a-zа-я]).{8,}$/;

    function inputNone(inputId) {
        $(`#${inputId}-input`).removeClass('invalid-input');
        $(`#${inputId}-input`).removeClass('valid-input');
        $(`#${inputId}-error-block`).removeClass('flash-error-visible');
    }

    function inputSuccess(inputId) {
        $(`#${inputId}-input`).removeClass('invalid-input');
        $(`#${inputId}-input`).removeClass('valid-input');
        $(`#${inputId}-input`).addClass('valid-input');
        $(`#${inputId}-error-block`).removeClass('flash-error-visible');
    }

    function inputError(inputId, error) {
        $(`#${inputId}-input`).removeClass('invalid-input');
        $(`#${inputId}-input`).removeClass('valid-input');
        $(`#${inputId}-input`).addClass('invalid-input');

        if (error === undefined) {
            return;
        }

        $(`#${inputId}-error`).text(getTranslation(errors, error));
        $(`#${inputId}-error-block`).removeClass('flash-error-visible');
        $(`#${inputId}-error-block`).addClass('flash-error-visible');
    }

    function checkWholeValidity() {
        let enabled = $('#key-input').val() !== "" &&
            $('#password-input').hasClass('valid-input') &&
            $('#repeat-password-input').hasClass('valid-input');

        $('#confirm-button').prop('disabled', !enabled);
    }

    $('#key-input').on('keyup keypress change input', () => {
        checkWholeValidity();
    });

    $('#password-input').on('keyup keypress change input', (event) => {
        let currentValue = $(event.target).val();

        if (currentValue === '') {
            inputNone('password');
            checkWholeValidity();
            return;
        }

        if (!passwordRegex.test(currentValue)) {
            inputError('password', 'password-format-incorrect');
            checkWholeValidity();
            return;
        }

        inputSuccess('password');
        $('#repeat-password-input').trigger('change');
        checkWholeValidity();
    });

    $('#repeat-password-input').on('keyup keypress change input', (event) => {
        let currentValue = $(event.target).val();

        if (currentValue === '') {
            inputNone('repeat-password');
            checkWholeValidity();
            return;
        }

        if (!passwordRegex.test(currentValue)) {
            inputError('repeat-password', 'password-format-incorrect');
            checkWholeValidity();
            return;
        }

        if (currentValue !== $('#password-input').val()) {
            inputError('repeat-password', 'repeat-password-incorrect');
            checkWholeValidity();
            return;
        }

        inputSuccess('repeat-password');
        checkWholeValidity();
    });

    checkWholeValidity();
});
