local base_domain = std.extVar('BASE_DOMAIN');

[
  {
    kind: 'Secret',
    apiVersion: 'v1',
    metadata: {
      name: 'pull-credentials',
    },
    data: {
      '.dockerconfigjson': std.base64(std.manifestJson({
        auths: {
          [std.extVar('CI_REGISTRY')]: {
            auth: std.base64(std.extVar('CI_DEPLOY_USER') + ':' + std.extVar('CI_DEPLOY_PASSWORD'))
          },
        },
      })),
    },
    type: 'kubernetes.io/dockerconfigjson',
  },

  {
    apiVersion: 'apps/v1',
    kind: 'Deployment',
    metadata: {
      name: 'website',
      labels: {
        app: 'website',
      },
      annotations: {
        'app.gitlab.com/app': std.extVar('CI_PROJECT_PATH_SLUG'),
        'app.gitlab.com/env': std.extVar('CI_ENVIRONMENT_SLUG'),
      }
    },
    spec: {
      selector: {
        matchLabels: {
          app: 'website',
        },
      },
      template: {
        metadata: {
          labels: {
            app: 'website',
          },
          annotations: {
            'app.gitlab.com/app': std.extVar('CI_PROJECT_PATH_SLUG'),
            'app.gitlab.com/env': std.extVar('CI_ENVIRONMENT_SLUG'),
          }
        },
        spec: {
          containers: [
            {
              name: 'backend',
              image: '%s/backend:%s' % [std.extVar('CI_REGISTRY_IMAGE'), std.extVar('CI_COMMIT_SHA')],
            },
            {
              name: 'proxy',
              image: '%s/proxy:%s' % [std.extVar('CI_REGISTRY_IMAGE'), std.extVar('CI_COMMIT_SHA')],
              ports: [
                {
                  containerPort: 3000,
                }
              ],
            },
          ],
          imagePullSecrets: [
            {
              name: 'pull-credentials'
            },
          ],
        },
      },
    },
  },

  {
    apiVersion: 'v1',
    kind: 'Service',
    metadata: {
      name: 'website',
    },
    spec: {
      selector: {
          app: 'website'
      },
      ports: [
        {
          name: 'http',
          protocol: 'TCP',
          port: 80,
          targetPort: 3000
        }
      ],
    },
  },

  {
    apiVersion: 'networking.k8s.io/v1beta1',
    kind: 'Ingress',
    metadata: {
      name: 'website'
    },
    spec: {
      rules: [
        {
          host: base_domain,
          http: {
            paths: [
              {
                path: '/',
                pathType: 'Prefix',
                backend: {
                  serviceName: 'website',
                  servicePort: 80,
                },
              },
            ],
          },
        },
      ],
    },
  },
]
