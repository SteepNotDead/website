<?php
declare(strict_types=1);
error_reporting(E_ALL);
set_error_handler(function ($severity, $message, $file, $line) {
    throw new ErrorException($message, $severity, $severity, $file, $line);
});

use Cristalix\Engine\MainHandler;

include_once __DIR__ . '/vendor/autoload.php';

spl_autoload_register(function ($class) {
    /** @noinspection PhpIncludeInspection */
    include_once 'core/' . str_replace('\\', '/', $class) . '.php';
});

$default_config = [
    'debug' => true
];

$config = [];

if (file_exists(__DIR__ . '/config.php')) {
    include_once __DIR__ . '/config.php';
    $config = array_merge($default_config, $config);
}

$handler = new MainHandler();

try {
    $handler->initialize($config);
} catch (Exception $e) {
    die('Caught error during initialization: ' . $e);
}

try {
    $handler->handle();
} catch (Exception $e) {
    if ($config['debug']) {
        die('Caught error during initialization: ' . $e);
    }
    http_response_code(500);
    die;
}