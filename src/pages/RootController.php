<?php

namespace Pages;

use Cristalix\Engine\BaseController;
use Cristalix\Engine\Extensions\DatabaseExtension;
use Cristalix\Engine\Extensions\RedisExtension;
use Cristalix\Engine\Extensions\SessionExtension;
use Cristalix\Engine\Request;
use Cristalix\Engine\RequestContext;
use Cristalix\Model\User;
use Twig\Environment;
use Exception;

class RootController extends BaseController
{
    use RedisExtension;
    use SessionExtension;
    use DatabaseExtension;

    private array $servers;

    public function initialize(array $config): void
    {
        $this->initializeSessions($config['sessions_redis']);
        $this->initializeRedis($config['redis']);
        $this->initializeDatabase($config['database']);
        $this->servers = $config['servers'];
    }

    public function processRequest(RequestContext $context): void
    {
        $path = $context->getRequest()->getPathParts();
        array_shift($path);
        if (empty($path)) {
            $this->processIndexPage($context);
            return;
        }
        if ($this->tryRenderStaticTemplate($context->getTwig(), $path, $context->getRequest())) {
            return;
        }
        if ($this->tryRenderStaticTemplate($context->getTwig(), ['404'], $context->getRequest())) {
            return;
        }
        die('\'Not found\' template is not found');
    }

    private function tryRenderStaticTemplate(Environment $twig, array $path, Request $request): bool
    {
        // add Cache-Control

        $staticTemplatePath = 'static/' . join('/', $path) . '.html';
        if (file_exists(TEMPLATES_PATH . $staticTemplatePath)) {
            if ($staticTemplatePath == 'static/servers/minigames.html') {
                $servers = [];
                try {
                    $data = $this->getRedis()->get("rating_entries");
                    $split = explode('@', $data);

                    $letters = array(' ', '&', '$');
                    foreach ($split as $value) {
                        $server = [];
                        $splitted = explode(':', $value);
                    

                        $server['name'] = $splitted[0];
                        $server['online'] = $splitted[1];
                        $server['link'] = strtolower(str_replace($letters, '', $splitted[0]));
                        $servers[] = $server;
                    }
                } catch(Exception $e) {
                    // ignored
                }
                $this->renderPage($twig, $staticTemplatePath, [
                    'get' => $request->getData(),
                    'post' => $request->postData(),
                    'user' => $this->getUser() != null ? $this->getUser()->getData() :
                        ($request->get('debug') !== null ? User::fetch($this->getDatabase(), 753043) : null),
                    'cookie_accept' => $request->getCookie('cookie_accept'),
                    'theme' => $request->getCookie('theme'),
                    'servers' => $servers
                ]);
                return true;
            }
            $this->renderPage($twig, $staticTemplatePath, [
                'get' => $request->getData(),
                'post' => $request->postData(),
                'user' => $this->getUser() != null ? $this->getUser()->getData() :
                    ($request->get('debug') !== null ? User::fetch($this->getDatabase(), 753043) : null),
                'cookie_accept' => $request->getCookie('cookie_accept'),
                'theme' => $request->getCookie('theme')
            ]);
            return true;
        }
        return false;
    }

    private function processIndexPage(RequestContext $context): void
    {
        $record = $this->getRedis()->get("online_record") + 0;
        $twigData = [
            'get' => $context->getRequest()->getData(),
            'post' => $context->getRequest()->postData(),
            'user' => $this->getUser() != null ? $this->getUser()->getData() : null,
            'cookie_accept' => $context->getRequest()->getCookie('cookie_accept'),
            'theme' => $context->getRequest()->getCookie('theme')
        ];

        $twigData['servers'] = [];
        $twigData['total_online'] = 0;
        $twigData['peak_online'] = $record;
        foreach ($this->servers as $id => $info) {
            $serverData = [];
            $serverData['id'] = $id;
            $serverData['name'] = $info['name'];

            $currentPlayers = $this->getRedis()->get("server_" . $id . "_current_players") + 0;
            $maxPlayers = $this->getRedis()->get("server_" . $id . "_max_players") + 0;
            $isOnline = $this->getRedis()->get("server_" . $id . "_is_online");

            if ($isOnline) {
                $twigData['total_online'] += $currentPlayers;
                $serverData['is_online'] = $isOnline;
                $serverData['current_players'] = $currentPlayers;
                $serverData['max_players'] = $maxPlayers;
            }

            $twigData['servers'][] = $serverData;
        }

        $this->renderPage($context->getTwig(), 'index.html', $twigData);
    }
}