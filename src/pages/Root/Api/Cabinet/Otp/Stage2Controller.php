<?php

namespace Pages\Root\Api\Cabinet\Otp;

use Cristalix\Engine\Extensions\ApiExtension;
use Cristalix\Engine\BaseController;
use Cristalix\Engine\Extensions\DatabaseExtension;
use Cristalix\Engine\Extensions\OTPExtension;
use Cristalix\Engine\RequestContext;
use Cristalix\Engine\Extensions\SessionExtension;
use Cristalix\Model\User;

class Stage2Controller extends BaseController
{
    use ApiExtension;
    use DatabaseExtension;
    use SessionExtension;
    use OTPExtension;

    public function initialize(array $config): void
    {
        $this->initializeSessions($config['sessions_redis']);
        $this->initializeDatabase($config['database']);
        $this->initializeOTP($config['otp_salt']);
    }

    public function processRequest(RequestContext $context): void
    {
        if (!$this->requireArgs($context, ['otp_code'])) {
            return;
        }

        if ($this->getUser() == null) {
            $this->error('unauthorized');
            return;
        }

        if ($this->getUser()->isOtpEnabled()) {
            $this->error('otp-already-enabled');
            return;
        }

        $otp_code = $context->getRequest()->post('otp_code');

        $secret_data = $this->getDatabase()->queryData("SELECT secret FROM ga_secrets WHERE user_id = :user_id", [
            ':user_id' => $this->getUser()->getId()
        ]);

        if (empty($secret_data)) {
            $this->error('stage-1-unavailable');
            return;
        }

        $secret = $secret_data[0]->secret;

        if (!$this->verifyOTP($this->getUser()->getId(), $secret, $otp_code)) {
            $this->error('wrong-otp-code');
            return;
        }

        $this->getDatabase()->query("UPDATE credentials SET ga_secret = :secret WHERE user_id = :user_id", [
            ':secret' => $secret,
            ':user_id' => $this->getUser()->getId()
        ]);

        $this->setUser(User::fetch($this->getDatabase(), $this->getUser()->getId()));
        $this->result([]);
    }
}