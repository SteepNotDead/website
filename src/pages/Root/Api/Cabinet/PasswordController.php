<?php

namespace Pages\Root\Api\Cabinet;

use Cristalix\Engine\Extensions\ApiExtension;
use Cristalix\Engine\BaseController;
use Cristalix\Engine\Extensions\DatabaseExtension;
use Cristalix\Engine\Extensions\PasswordVerificationExtension;
use Cristalix\Engine\RequestContext;
use Cristalix\Engine\Extensions\SessionExtension;
use Cristalix\Engine\Extensions\FormatVerificationExtension;

class PasswordController extends BaseController
{
    use ApiExtension;
    use DatabaseExtension;
    use SessionExtension;
    use PasswordVerificationExtension;
    use FormatVerificationExtension;

    public function initialize(array $config): void
    {
        $this->initializeSessions($config['sessions_redis']);
        $this->initializeDatabase($config['database']);
        $this->initializePasswords($config['password_salt']);
    }

    public function processRequest(RequestContext $context): void
    {
        if (!$this->requireArgs($context, ['old_password', 'new_password'])) {
            return;
        }

        if ($this->getUser() == null) {
            $this->error('unauthorized');
            return;
        }

        $old_password = $context->getRequest()->post('old_password');
        $new_password = $context->getRequest()->post('new_password');

        if (!$this->isPasswordCorrect($new_password)) {
            $this->error('password-format-incorrect');
            return;
        }

        $old_credentials = $this->getDatabase()->queryData("SELECT password_hash FROM credentials WHERE user_id = :user_id", [
            ':user_id' => $this->getUser()->getId()
        ])[0];

        if (!$this->verifyPassword($this->getUser()->getId(), $old_credentials->password_hash, $old_password)) {
            $this->error('wrong-password');
            return;
        }

        $this->getDatabase()->query("UPDATE credentials SET password_hash = :password_hash WHERE user_id = :user_id", [
            ':user_id' => $this->getUser()->getId(),
            ':password_hash' => $this->getPasswordHash($this->getUser()->getId(), $new_password)
        ]);

        $this->result([]);
    }
}