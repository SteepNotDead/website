<?php

namespace Pages\Root\Api\Cabinet;

use Cristalix\Engine\Extensions\ApiExtension;
use Cristalix\Engine\BaseController;
use Cristalix\Engine\Extensions\AvailabilityVerificationExtension;
use Cristalix\Engine\Extensions\DatabaseExtension;
use Cristalix\Engine\Extensions\DonateExtension;
use Cristalix\Engine\Extensions\PasswordVerificationExtension;
use Cristalix\Engine\Extensions\XenforoApiExtension;
use Cristalix\Engine\RequestContext;
use Cristalix\Engine\Extensions\SessionExtension;
use Cristalix\Engine\Extensions\FormatVerificationExtension;
use Cristalix\Model\User;
use Exception;

class UsernameController extends BaseController
{
    use ApiExtension;
    use DatabaseExtension;
    use SessionExtension;
    use DonateExtension;
    use FormatVerificationExtension;
    use PasswordVerificationExtension;
    use AvailabilityVerificationExtension;
    use XenforoApiExtension;

    private int $username_change_price;

    public function initialize(array $config): void
    {
        $this->initializeSessions($config['sessions_redis']);
        $this->initializeDatabase($config['database']);
        $this->initializeDonate($config['tower_rest_api_url']);
        $this->initializePasswords($config['password_salt']);
        $this->initializeXenforoApi($config['forum_api']);

        $this->username_change_price = $config['prices']['username_change'];
    }

    public function processRequest(RequestContext $context): void
    {
        if (!$this->requireArgs($context, ['password', 'new_username'])) {
            return;
        }

        if ($this->getUser() == null) {
            $this->error('unauthorized');
            return;
        }

        $password = $context->getRequest()->post('password');
        $new_username = $context->getRequest()->post('new_username');

        if (!$this->isUsernameCorrect($new_username)) {
            $this->error('username-format-incorrect');
            return;
        }

        if (!$this->isUsernameAvailable($new_username)) {
            $this->error('username-unavailable');
            return;
        }

        $last_username_change_time = $this->getDatabase()->queryData(
            "SELECT timestamp FROM username_log WHERE user_id = :user_id ORDER BY timestamp DESC LIMIT 1", [
                ':user_id' => $this->getUser()->getId()
        ]);

        $last_username_change_time = empty($last_username_change_time) ? 0 : $last_username_change_time[0]->timestamp;

        if ($last_username_change_time + 86400 >= time()) {
            $this->error('cant-change-username-time');
            return;
        }

        $credentials = $this->getDatabase()->queryData("SELECT password_hash FROM credentials WHERE user_id = :user_id", [
            ':user_id' => $this->getUser()->getId()
        ])[0];

        if (!$this->verifyPassword($this->getUser()->getId(), $credentials->password_hash, $password)) {
            $this->error('wrong-password');
            return;
        }

        if (($pay_error = $this->withdrawBalance($this->getUser()->getUuid(),
                $this->username_change_price, 'Смена никнейма')) != null) {
            $this->error($pay_error);
            return;
        }

        /*
        try {
            $this->changeForumUserUsername($this->getUser()->getForumUserId(), $new_username);
        } catch (Exception $e) {
            error_log($e);
            $this->error('internal-server-error');
            return;
        }
        */

        $this->getDatabase()->query("INSERT INTO username_log (user_id, old_username, new_username) VALUES (:user_id, :old_username, :new_username)", [
            ':user_id' => $this->getUser()->getId(),
            ':old_username' => $this->getUser()->getUsername(),
            ':new_username' => $new_username
        ]);

        $this->getDatabase()->query("UPDATE users SET username = :new_username WHERE id = :user_id", [
            ':new_username' => $new_username,
            ':user_id' => $this->getUser()->getId()
        ]);

        $this->setUser(User::fetch($this->getDatabase(), $this->getUser()->getId()));
        $this->result([]);
    }
}