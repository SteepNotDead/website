<?php

namespace Pages\Root\Api\Cabinet;

use Cristalix\Engine\Extensions\ApiExtension;
use Cristalix\Engine\BaseController;
use Cristalix\Engine\Extensions\DatabaseExtension;
use Cristalix\Engine\RequestContext;
use Cristalix\Engine\Extensions\SessionExtension;
use Cristalix\Engine\Extensions\PermissionsExtension;
use Cristalix\Model\User;

class NameColorController extends BaseController
{
    use ApiExtension;
    use DatabaseExtension;
    use PermissionsExtension;
    use SessionExtension;

    const PERMISSION = 'site.namecolor';

    public function initialize(array $config): void
    {
        $this->initializeSessions($config['sessions_redis']);
        $this->initializeDatabase($config['database']);
        $this->initializePermissions($config['groups']);
    }

    public function processRequest(RequestContext $context): void
    {
        if (!$this->requireArgs($context, ['color'])) {
            return;
        }

        if ($this->getUser() == null) {
            $this->error('unauthorized');
            return;
        }

        if (!in_array(self::PERMISSION, $this->getPermissions($this->getUser()))) {
            $this->error('no-permissions');
            return;
        }

        $pattern = '/((?![0-9a-f]).)*/i';
        $color = preg_replace($pattern, '', $context->getRequest()->post('color'));
        $ser_color = hexdec($color);

        $this->getDatabase()->query("UPDATE users SET color = :new_color WHERE id = :user_id", [
            ':new_color' => $ser_color,
            ':user_id' => $this->getUser()->getId()
        ]);

        $this->setUser(User::fetch($this->getDatabase(), $this->getUser()->getId()));
        $this->result([]);
    }

}