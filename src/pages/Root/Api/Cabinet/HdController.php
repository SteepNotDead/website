<?php

namespace Pages\Root\Api\Cabinet;

use Cristalix\Engine\Extensions\ApiExtension;
use Cristalix\Engine\BaseController;
use Cristalix\Engine\Extensions\DatabaseExtension;
use Cristalix\Engine\RequestContext;
use Cristalix\Engine\Extensions\SessionExtension;
use Cristalix\Engine\Extensions\DonateExtension;
use Cristalix\Model\User;

class HdController extends BaseController
{
    use ApiExtension;
    use DonateExtension;
    use DatabaseExtension;
    use SessionExtension;

    private array $hd_options;

    public function initialize(array $config): void
    {
        $this->initializeSessions($config['sessions_redis']);
        $this->initializeDatabase($config['database']);
        $this->initializeDonate($config['tower_rest_api_url']);

        $this->hd_options = $config['hd_options'];
    }

    public function processRequest(RequestContext $context): void
    {
        if (!$this->requireArgs($context, ['hd_time'])) {
            return;
        }

        if ($this->getUser() == null) {
            $this->error('unauthorized');
            return;
        }

        $time = $context->getRequest()->post('hd_time');
        if (!array_key_exists($time, $this->hd_options)) {
            $this->error('bad-hd-time');
            return;
        }

        $time_data = $this->hd_options[$time];
        if (($pay_error = $this->withdrawBalance($this->getUser()->getUuid(),
                $time_data['price'], 'Продление HD подписки на ' . $time_data['name'])) != null) {
            $this->error($pay_error);
            return;
        }
        $query = "UPDATE hd_subscriptions SET expires = greatest(now(), expires)::timestamp + INTERVAL '". $time_data['interval'] ."' WHERE user_id = :user_id;";

        $this->getDatabase()->query($query, [
            ':user_id' => $this->getUser()->getId()
        ]);

        $this->setUser(User::fetch($this->getDatabase(), $this->getUser()->getId()));
        $this->result([]);
    }

}