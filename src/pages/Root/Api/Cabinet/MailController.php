<?php


namespace Pages\Root\Api\Cabinet;

use Cristalix\Engine\Extensions\ApiExtension;
use Cristalix\Engine\BaseController;
use Cristalix\Engine\Extensions\AvailabilityVerificationExtension;
use Cristalix\Engine\Extensions\DatabaseExtension;
use Cristalix\Engine\Extensions\FormatVerificationExtension;
use Cristalix\Engine\Extensions\MailExtension;
use Cristalix\Engine\Extensions\PasswordVerificationExtension;
use Cristalix\Engine\Extensions\RecaptchaExtension;
use Cristalix\Engine\RequestContext;
use Cristalix\Engine\Extensions\SessionExtension;
use Cristalix\Model\User;
use Exception;

class MailController extends BaseController
{
    use ApiExtension;
    use DatabaseExtension;
    use RecaptchaExtension;
    use MailExtension;
    use SessionExtension;
    use PasswordVerificationExtension;
    use FormatVerificationExtension;
    use AvailabilityVerificationExtension;

    public function initialize(array $config): void
    {
        $this->initializeSessions($config['sessions_redis']);
        $this->initializeDatabase($config['database']);
        $this->initializeRecaptcha($config['recaptcha_key']);
        $this->initializeMail($config['mail']);
        $this->initializePasswords($config['password_salt']);
    }

    public function processRequest(RequestContext $context): void
    {
        if (!$this->requireArgs($context, ['password', 'new_email'])) {
            return;
        }

        if ($this->getUser() == null) {
            $this->error('unauthorized');
            return;
        }

        $password = $context->getRequest()->post('password');
        $new_email = $context->getRequest()->post('new_email');

        if (!$this->isEmailCorrect($new_email)) {
            $this->error('email-format-incorrect');
            return;
        }

        if (!$this->isEmailAvailable($new_email)) {
            $this->error('email-unavailable');
            return;
        }

        $credentials = $this->getDatabase()->queryData("SELECT password_hash FROM credentials WHERE user_id = :user_id", [
            ':user_id' => $this->getUser()->getId()
        ])[0];

        if (!$this->verifyPassword($this->getUser()->getId(), $credentials->password_hash, $password)) {
            $this->error('wrong-password');
            return;
        }

        try {
            $key = bin2hex(random_bytes(8));
        } catch (Exception $e) {
            error_log($e);
            $this->error('internal-server-error');
            return;
        }

        $this->getDatabase()->query("DELETE FROM mail_changes WHERE expires <= to_timestamp(:time) OR user_id = :user_id", [
            ':time' => time(),
            ':user_id' => $this->getUser()->getId()
        ]);

        $this->getDatabase()->query("INSERT INTO mail_changes (user_id, email, \"key\", expires) VALUES (:user_id, :email, :key, to_timestamp(:expires))", [
            ':user_id' => $this->getUser()->getId(),
            ':email' => $new_email,
            ':key' => $key,
            ':expires' => time() + 86400
        ]);

        try {
            $contents = $context->getTwig()->render('mail/changemail.html', [
                'username' => $this->getUser()->getUsername(),
                'email' => $new_email,
                'key' => $key
            ]);
        } catch (Exception $e) {
            error_log($e);
            $this->error('internal-server-error');
            return;
        }

        try {
            $this->send($new_email, 'Cristalix - Смена почты', $contents);
        } catch (Exception $e) {
            error_log($e);
            $this->error('unable-to-send-email');
            return;
        }

        $this->result([]);
    }
}