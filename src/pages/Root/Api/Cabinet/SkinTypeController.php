<?php

namespace Pages\Root\Api\Cabinet;

use Cristalix\Engine\Extensions\ApiExtension;
use Cristalix\Engine\BaseController;
use Cristalix\Engine\Extensions\DatabaseExtension;
use Cristalix\Engine\RequestContext;
use Cristalix\Engine\Extensions\SessionExtension;
use Cristalix\Model\User;

class SkinTypeController extends BaseController
{
    use ApiExtension;
    use DatabaseExtension;
    use SessionExtension;

    public function initialize(array $config): void
    {
        $this->initializeSessions($config['sessions_redis']);
        $this->initializeDatabase($config['database']);
    }

    public function processRequest(RequestContext $context): void
    {
        if (!$this->requireArgs($context, ['skin_type'])) {
            return;
        }

        if ($this->getUser() == null) {
            $this->error('unauthorized');
            return;
        }

        $skin_type = $context->getRequest()->post('skin_type');
        if ($skin_type !== 'alex' && $skin_type !== 'steve') {
            $this->error('bad-skin-type');
            return;
        }
        $serialized_skin_type = $skin_type === 'steve' ? 'true' : 'false';

        $this->getDatabase()->query("UPDATE users SET skin_model = :skin_model WHERE id = :user_id", [
            ':skin_model' => $serialized_skin_type,
            ':user_id' => $this->getUser()->getId()
        ]);

        $this->setUser(User::fetch($this->getDatabase(), $this->getUser()->getId()));
        $this->result([]);
    }
}