<?php

namespace Pages\Root\Api\Cabinet\Mail;

use Cristalix\Engine\Extensions\ApiExtension;
use Cristalix\Engine\BaseController;
use Cristalix\Engine\Extensions\DatabaseExtension;
use Cristalix\Engine\Extensions\XenforoApiExtension;
use Cristalix\Engine\RequestContext;
use Cristalix\Engine\Extensions\SessionExtension;
use Cristalix\Model\User;

class ConfirmController extends BaseController
{
    use ApiExtension;
    use SessionExtension;
    use DatabaseExtension;
    use XenforoApiExtension;

    public function initialize(array $config): void
    {
        $this->initializeSessions($config['sessions_redis']);
        $this->initializeDatabase($config['database']);
        $this->initializeXenforoApi($config['forum_api']);
    }

    public function processRequest(RequestContext $context): void
    {
        if (!$this->requireArgs($context, ['key'])) {
            return;
        }

        $key = $context->getRequest()->post('key');

        $mail_change_data = $this->getDatabase()->queryData("SELECT * FROM mail_changes WHERE \"key\" = :key AND expires > to_timestamp(:time)", [
            ':key' => $key,
            ':time' => time()
        ]);

        if (empty($mail_change_data)) {
            $this->error('wrong-key');
            return;
        }

        $data = $mail_change_data[0];

        try {
            $this->changeForumUserEmail($this->getUser()->getForumUserId(), $data->email);
        } catch (Exception $e) {
            error_log($e);
            $this->error('internal-server-error');
            return;
        }

        $this->getDatabase()->query("DELETE FROM mail_changes WHERE \"key\" = :key", [
            ':key' => $key
        ]);

        $this->getDatabase()->query("UPDATE credentials SET email = :email WHERE user_id = :user_id", [
            ':user_id' => $data->user_id,
            ':email' => $data->email
        ]);

        $this->setUser(User::fetch($this->getDatabase(), $this->getUser()->getId()));
        $this->result([]);
    }
}