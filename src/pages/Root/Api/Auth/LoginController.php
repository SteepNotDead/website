<?php

namespace Pages\Root\Api\Auth;

use Cristalix\Engine\Extensions\ApiExtension;
use Cristalix\Engine\BaseController;
use Cristalix\Engine\Extensions\DatabaseExtension;
use Cristalix\Engine\Extensions\PasswordVerificationExtension;
use Cristalix\Engine\Extensions\RecaptchaExtension;
use Cristalix\Engine\RequestContext;
use Cristalix\Engine\Extensions\SessionExtension;
use Cristalix\Engine\Extensions\OTPExtension;
use Cristalix\Model\User;

class LoginController extends BaseController
{
    use ApiExtension;
    use DatabaseExtension;
    use RecaptchaExtension;
    use OTPExtension;
    use SessionExtension;
    use PasswordVerificationExtension;

    public function initialize(array $config): void
    {
        $this->initializeSessions($config['sessions_redis']);
        $this->initializeDatabase($config['database']);
        $this->initializeRecaptcha($config['recaptcha_key']);
        $this->initializeOTP($config['otp_salt']);
        $this->initializePasswords($config['password_salt']);
    }

    public function processRequest(RequestContext $context): void
    {
        if (!$this->requireArgs($context, ['username', 'password', 'captcha'])) {
            return;
        }

        if (!$this->checkCaptcha($context->getRequest()->post('captcha'),
            $context->getRequest()->getRealIP(), 'login')) {
            $this->error('captcha-failed');
            return;
        }

        $username = $context->getRequest()->post('username');
        $password = $context->getRequest()->post('password');

        $username_column = strpos($username, '@') !== false ? 'credentials.email' :
            'users.username';

        $login_results = $this->getDatabase()->queryData("SELECT users.id, users.username, credentials.password_hash,
                                    credentials.ga_secret AS ga_secret FROM users 
                                    LEFT JOIN credentials ON users.id = credentials.user_id
                                    LEFT JOIN ga_secrets gs on users.id = gs.user_id
                                    WHERE $username_column = :username", [
            ':username' => $username
        ]);

        if (empty($login_results)) {
            $this->error('login-failed');
            return;
        }

        $login_result = $login_results[0];

        if (!$this->verifyPassword($login_result->id, $login_result->password_hash, $password)) {
            $this->error('login-failed');
            return;
        }

        if ($login_result->ga_secret != null) {
            if ($context->getRequest()->post('otp') === null) {
                $this->error('otp-required');
                return;
            }

            if (!$this->verifyOTP($login_result->id, $login_result->ga_secret, $context->getRequest()->post('otp'))) {
                $this->error('otp-incorrect');
                return;
            }
        }

        $this->setUser(User::fetch($this->getDatabase(), $login_result->id));
        $this->result([]);
    }
}