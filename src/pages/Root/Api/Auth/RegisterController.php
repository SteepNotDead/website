<?php

namespace Pages\Root\Api\Auth;

use Cristalix\Engine\Extensions\ApiExtension;
use Cristalix\Engine\BaseController;
use Cristalix\Engine\Extensions\AvailabilityVerificationExtension;
use Cristalix\Engine\Extensions\DatabaseExtension;
use Cristalix\Engine\Extensions\MailExtension;
use Cristalix\Engine\Extensions\RecaptchaExtension;
use Cristalix\Engine\RequestContext;
use Cristalix\Engine\Extensions\FormatVerificationExtension;
use Exception;

class RegisterController extends BaseController
{
    use ApiExtension;
    use DatabaseExtension;
    use RecaptchaExtension;
    use MailExtension;
    use FormatVerificationExtension;
    use AvailabilityVerificationExtension;

    public function initialize(array $config): void
    {
        $this->initializeDatabase($config['database']);
        $this->initializeRecaptcha($config['recaptcha_key']);
        $this->initializeMail($config['mail']);
    }

    public function processRequest(RequestContext $context): void
    {
        if (!$this->requireArgs($context, ['username', 'email', 'password', 'mojang_username', 'captcha'])) {
            return;
        }

        if (!$this->checkCaptcha($context->getRequest()->post('captcha'),
            $context->getRequest()->getRealIP(), 'register')) {
            $this->error('captcha-failed');
            return;
        }

        $username = $context->getRequest()->post('username');
        $email = $context->getRequest()->post('email');
        $password = $context->getRequest()->post('password');
        $mojang_username = $context->getRequest()->post('mojang_username');

        $this->getDatabase()->query("DELETE FROM registrations WHERE expires <= to_timestamp(:time)", [
            ':time' => time()
        ]);

        if (!$this->isUsernameCorrect($username)) {
            $this->error('username-format-incorrect');
            return;
        }

        if (!$this->isEmailCorrect($email)) {
            $this->error('email-format-incorrect');
            return;
        }

        if (!$this->isPasswordCorrect($password)) {
            $this->error('password-format-incorrect');
            return;
        }

        /*
        if (!$this->checkGender($gender)) {
            $this->error('gender-incorrect');
            return;
        }
        */

        if (!$this->isUsernameAvailable($username)) {
            $this->error('username-unavailable');
            return;
        }

        if (!$this->isEmailAvailable($email)) {
            $this->error('email-unavailable');
            return;
        }

        if ($mojang_username != '' && !$this->doesMojangUsernameExist($mojang_username)) {
            $this->error('mojang-username-not-exist');
            return;
        }

        try {
            $key = bin2hex(random_bytes(8));
        } catch (Exception $e) {
            error_log($e);
            $this->error('internal-server-error');
            return;
        }

        try {
            $contents = $context->getTwig()->render('mail/register.html', [
                'username' => $username,
                'email' => $email,
                'key' => $key
            ]);
        } catch (Exception $e) {
            error_log($e);
            $this->error('internal-server-error');
            return;
        }

        try {
            $this->send($email, 'Cristalix - Регистрация', $contents);
        } catch (Exception $e) {
            error_log($e);
            $this->error('unable-to-send-email');
            return;
        }

        $this->getDatabase()->query("INSERT INTO registrations (username, password, email, mojang_username, \"key\", expires) 
                                        VALUES (:username, :password, :email, :mojang_username, :key, to_timestamp(:expires))", [
            ':username' => $username,
            ':password' => $password,
            ':email' => $email,
            ':mojang_username' => $mojang_username == '' ? null : $mojang_username,
            ':key' => $key,
            ':expires' => time() + 86400
        ]);

        $this->result([]);
    }

}