<?php


namespace Pages\Root\Api\Auth;

use Cristalix\Engine\Extensions\ApiExtension;
use Cristalix\Engine\BaseController;
use Cristalix\Engine\Extensions\DatabaseExtension;
use Cristalix\Engine\Extensions\MailExtension;
use Cristalix\Engine\Extensions\RecaptchaExtension;
use Cristalix\Engine\RequestContext;
use Exception;

class RecoveryController extends BaseController
{
    use ApiExtension;
    use DatabaseExtension;
    use RecaptchaExtension;
    use MailExtension;

    public function initialize(array $config): void
    {
        $this->initializeDatabase($config['database']);
        $this->initializeRecaptcha($config['recaptcha_key']);
        $this->initializeMail($config['mail']);
    }

    public function processRequest(RequestContext $context): void
    {
        if (!$this->requireArgs($context, ['email', 'captcha'])) {
            return;
        }

        if (!$this->checkCaptcha($context->getRequest()->post('captcha'),
            $context->getRequest()->getRealIP(), 'password_recovery')) {
            $this->error('captcha-failed');
            return;
        }

        $email = $context->getRequest()->post('email');

        $login_results = $this->getDatabase()->queryData("SELECT credentials.* FROM credentials
                                    WHERE email = :email", [
            ':email' => $email
        ]);

        if (empty($login_results)) {
            $this->result([]);
            return;
        }

        $user_info = $this->getDatabase()->queryData("SELECT * FROM users WHERE id = :id", [
            ':id' => $login_results[0]->user_id
        ]);

        $user = $user_info[0];

        try {
            $key = bin2hex(random_bytes(8));
        } catch (Exception $e) {
            error_log($e);
            $this->result([]);
            return;
        }

        $this->getDatabase()->query("DELETE FROM password_recoveries WHERE expires <= to_timestamp(:time) OR user_id = :user_id", [
            ':time' => time(),
            ':user_id' => $user->id
        ]);

        $this->getDatabase()->query("INSERT INTO password_recoveries (user_id, \"key\", expires) VALUES (:user_id, :key, to_timestamp(:expires))", [
            ':user_id' => $user->id,
            ':key' => $key,
            ':expires' => time() + 86400
        ]);

        try {
            $contents = $context->getTwig()->render('mail/recovery.html', [
                'username' => $user->username,
                'email' => $email,
                'key' => $key
            ]);
        } catch (Exception $e) {
            error_log($e);
            $this->result([]);
            return;
        }

        try {
            $this->send($email, 'Cristalix - Восстановление пароля', $contents);
        } catch (Exception $e) {
            error_log($e);
            $this->result([]);
            return;
        }

        $this->result([]);
    }
}