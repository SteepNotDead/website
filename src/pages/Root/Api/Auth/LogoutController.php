<?php

namespace Pages\Root\Api\Auth;

use Cristalix\Engine\Extensions\ApiExtension;
use Cristalix\Engine\BaseController;
use Cristalix\Engine\Extensions\DatabaseExtension;
use Cristalix\Engine\Extensions\RecaptchaExtension;
use Cristalix\Engine\RequestContext;
use Cristalix\Engine\Extensions\SessionExtension;
use Cristalix\Engine\Extensions\OTPExtension;
use Cristalix\Model\User;

class LogoutController extends BaseController
{
    use ApiExtension;
    use SessionExtension;

    public function initialize(array $config): void
    {
        $this->initializeSessions($config['sessions_redis']);
    }

    public function processRequest(RequestContext $context): void
    {
        $this->setUser(null);
        $this->result([]);
    }
}