<?php

namespace Pages\Root\Api\Auth\Recovery;

use Cristalix\Engine\Extensions\ApiExtension;
use Cristalix\Engine\BaseController;
use Cristalix\Engine\Extensions\DatabaseExtension;
use Cristalix\Engine\Extensions\PasswordVerificationExtension;
use Cristalix\Engine\RequestContext;
use Cristalix\Engine\Extensions\FormatVerificationExtension;

class ConfirmController extends BaseController
{
    use ApiExtension;
    use DatabaseExtension;
    use FormatVerificationExtension;
    use PasswordVerificationExtension;

    public function initialize(array $config): void
    {
        $this->initializeDatabase($config['database']);
        $this->initializePasswords($config['password_salt']);
    }

    public function processRequest(RequestContext $context): void
    {
        if (!$this->requireArgs($context, ['key', 'new_password'])) {
            return;
        }

        $key = $context->getRequest()->post('key');
        $new_password = $context->getRequest()->post('new_password');

        if (!$this->isPasswordCorrect($new_password)) {
            $this->error('password-format-incorrect');
            return;
        }

        $reset_password_data = $this->getDatabase()->queryData("SELECT * FROM password_recoveries WHERE \"key\" = :key AND expires > to_timestamp(:time)", [
            ':key' => $key,
            ':time' => time()
        ]);

        if (empty($reset_password_data)) {
            $this->error('wrong-key');
            return;
        }

        $this->getDatabase()->query("DELETE FROM password_recoveries WHERE \"key\" = :key", [
            ':key' => $key
        ]);

        $data = $reset_password_data[0];

        $this->getDatabase()->query("UPDATE credentials SET password_hash = :password_hash WHERE user_id = :user_id", [
            ':user_id' => $data->user_id,
            ':password_hash' => $this->getPasswordHash($data->user_id, $new_password)
        ]);

        $this->result([]);
    }
}