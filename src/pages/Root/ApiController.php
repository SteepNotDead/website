<?php

namespace Pages\Root;

use Cristalix\Engine\Extensions\ApiExtension;
use Cristalix\Engine\BaseController;
use Cristalix\Engine\RequestContext;

class ApiController extends BaseController
{
    use ApiExtension;

    public function processRequest(RequestContext $context): void
    {
        $this->error('unsupported-method');
    }
}