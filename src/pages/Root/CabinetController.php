<?php

namespace Pages\Root;

use Cristalix\Engine\BaseController;
use Cristalix\Engine\RequestContext;
use Cristalix\Engine\Extensions\SessionExtension;

class CabinetController extends BaseController
{
    use SessionExtension;

    private int $username_change_price;
    private array $hd_options;

    public function initialize(array $config): void
    {
        $this->initializeSessions($config['sessions_redis']);
        $this->username_change_price = $config['prices']['username_change'];
        $this->hd_options = $config['hd_options'];
    }

    public function processRequest(RequestContext $context): void
    {
        $path = $context->getRequest()->getPathParts();
        array_shift($path);
        array_shift($path);
        if ($this->getUser() != null) {
            $this->renderPage($context->getTwig(), 'cabinet/main.html', [
                'user' => $this->getUser()->getData(),
                'username_change_price' => $this->username_change_price,
                'hd_opts' => $this->hd_options,
                'cookie_accept' => $context->getRequest()->getCookie('cookie_accept'),
                'theme' => $context->getRequest()->getCookie('theme')
            ]);
            return;
        }

        if (count($path) > 0 && $path[0] == 'login') {
            $this->renderPage($context->getTwig(), 'cabinet/login.html', [
                'cookie_accept' => $context->getRequest()->getCookie('cookie_accept'),
                'theme' => $context->getRequest()->getCookie('theme')
            ]);
            return;
        }

        header('location: /cabinet/login');
        die;
    }
}