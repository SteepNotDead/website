<?php

namespace Cristalix\Engine;

use Exception;
use Twig\Environment;

abstract class BaseController
{
    public function initialize(array $config): void
    {
    }

    abstract public function processRequest(RequestContext $context): void;

    protected function renderPage(Environment $twig, string $template_name, array $template_data = []): void
    {
        try {
            echo $twig->render($template_name, $template_data);
        } catch (Exception $e) {
            error_log($e);
			//die($e);
            die('<pre>Failed to render page! Check log!</pre>');
        }
    }
}