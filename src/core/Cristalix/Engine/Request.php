<?php

namespace Cristalix\Engine;

class Request
{
    private array $path_parts;
    private array $get_data;
    private array $post_data;
    private array $file_data;
    private array $cookie_data;
    private string $type;
    private string $real_ip;

    public function getData(): array
    {
        return $this->get_data;
    }

    public function postData(): array
    {
        return $this->post_data;
    }

    public function getCookie(string $parameter): ?string
    {
        return isset($this->cookie_data[$parameter]) ? strval($this->cookie_data[$parameter]) : null;
    }

    public function get(string $parameter): ?string
    {
        return isset($this->get_data[$parameter]) ? strval($this->get_data[$parameter]) : null;
    }

    public function post(string $parameter): ?string
    {
        return isset($this->post_data[$parameter]) ? strval($this->post_data[$parameter]) : null;
    }

    public function file(string $name): ?array
    {
        return isset($this->file_data[$name]) ? $this->file_data[$name] : null;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getPath(): string
    {
        return join('/', $this->path_parts);
    }

    public function getPathParts(): array
    {
        return $this->path_parts;
    }

    public static function createFromGlobalVars(): Request
    {
        global $_GET;
        global $_POST;
        global $_FILES;
        global $_SERVER;
        global $_COOKIE;

        $request = new Request();
        $path = 'root';
        if (isset($_GET['action'])) {
            $path .= '/' . $_GET['action'];
            unset($_GET['action']);
        }
        $request->path_parts = explode('/', $path);
        $request->path_parts = array_filter($request->path_parts, function ($value) {
            return $value !== '' && $value != '.' && $value != '..';
        });
        $request->get_data = $_GET;
        $request->post_data = $_POST;
        $request->file_data = $_FILES;
        $request->cookie_data = $_COOKIE;
        $request->type = $_SERVER['REQUEST_METHOD'];
        $request->real_ip = $_SERVER['REMOTE_ADDR'];
        return $request;
    }

    public function getRealIP(): string
    {
        return $this->real_ip;
    }
}