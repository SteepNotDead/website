<?php

namespace Cristalix\Engine;

use PDO;
use PDOException;
use RuntimeException;

class Database
{
    private PDO $driver;

    public function __construct(array $config)
    {
        $this->driver = new PDO(sprintf("pgsql:dbname=%s;host=%s", $config['database'], $config['hostname']), $config['username'], $config['password']);
        $this->driver->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function query(string $sql, array $params = []): void
    {
        $stmt = $this->driver->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
        try {
            $stmt->execute($params);
        } catch (PDOException $e) {
            error_log($e);
            error_log(print_r($stmt->errorInfo(), true));
            throw new RuntimeException("Exception occurred while querying database");
        }
    }

    public function queryData(string $sql, array $params = []): array
    {
        $stmt = $this->driver->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
        try {
            $stmt->execute($params);
        } catch (PDOException $e) {
            error_log($e);
            error_log(print_r($stmt->errorInfo(), true));
            throw new RuntimeException("Exception occurred while querying data from database");
        }
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }
}