<?php

namespace Cristalix\Engine;

use Twig\Environment;

class RequestContext
{
    private Request $request;
    private Environment $twig;

    public function __construct(Request $request, Environment $twig)
    {
        $this->request = $request;
        $this->twig = $twig;
    }

    public function getRequest(): Request
    {
        return $this->request;
    }

    public function getTwig(): Environment
    {
        return $this->twig;
    }
}