<?php


namespace Cristalix\Engine\Extensions;

trait DonateExtension
{
    use TowerExtension;

    protected function initializeDonate(string $tower_rest_api_url): void
    {
        $this->initializeTower($tower_rest_api_url);
    }

    protected function withdrawBalance(string $uuid, int $amount, string $description): ?string
    {
        $response = $this->requestPackage(
            'pw.lach.cristalix.core.invoice.packages.MoneyTransactionRequestPackage', (object)[
            'user' => $uuid,
            'price' => $amount,
            'bonuses' => false,
            'description' => $description
        ], 15000, 'pw.lach.cristalix.core.invoice.packages.MoneyTransactionResponsePackage');
        return $response->errorMessage;
    }
}