<?php

namespace Cristalix\Engine\Extensions;

use Redis;
use RedisException;

trait RedisExtension
{
    private bool $redis_ready = false;
    private array $redis_config;
    private Redis $redis;

    protected function initializeRedis(array $config): void
    {
        $this->redis_config = $config;
    }

    protected function getRedis(): Redis
    {
        if (!$this->redis_ready) {
            $this->redis = new Redis();
            $this->redis->connect($this->redis_config['hostname'], $this->redis_config['port']);
            if ($this->redis_config['password'] != null) {
                $this->redis->auth($this->redis_config['password']);
            }
            try {
                if (!$this->redis->ping()) {
                    die('Failed to connect to redis');
                }
            } catch (RedisException $e) {
                die('Failed to connect to redis');
            }
            $this->redis_ready = true;
        }
        return $this->redis;
    }
}