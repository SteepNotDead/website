<?php


namespace Cristalix\Engine\Extensions;


use Exception;
use PHPMailer\PHPMailer\PHPMailer;

trait MailExtension
{
    private array $mail_config;
    private PHPMailer $mailer;

    protected function initializeMail(array $config): void
    {
        $this->mail_config = $config;
        $mailer = new PHPMailer(true);
        $mailer->isSMTP();
        $mailer->CharSet = PHPMailer::CHARSET_UTF8;
        $mailer->Encoding = PHPMailer::ENCODING_BASE64;
        $mailer->Host = $config['host'];
        $mailer->SMTPAuth = true;
        $mailer->Username = $config['username'];
        $mailer->Password = $config['password'];
        if ($config['secure']) {
            $mailer->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
        }
        $mailer->Port = $config['port'];
        $this->mailer = $mailer;
    }

    /**
     * @param string $destination
     * @param string $subject
     * @param string $contents
     * @throws Exception
     */
    protected function send(string $destination, string $subject, string $contents): void
    {
        $this->mailer->setFrom($this->mail_config['from']);
        $this->mailer->addAddress($destination);
        $this->mailer->Subject = $subject;
        $this->mailer->isHTML(true);
        $this->mailer->Body = $contents;

        $this->mailer->send();
    }
}