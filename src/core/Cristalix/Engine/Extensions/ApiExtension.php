<?php

namespace Cristalix\Engine\Extensions;

use Cristalix\Engine\RequestContext;

trait ApiExtension
{
    protected function requireArgs(RequestContext $context, array $arguments): bool
    {
        foreach ($arguments as $argument) {
            if ($context->getRequest()->post($argument) === null) {
                $this->error("no argument named '$argument'");
                return false;
            }
        }
        return true;
    }

    protected function error(string $error): void
    {
        header('Content-Type: application/json');
        echo json_encode([
            'success' => false,
            'error' => $error
        ]);
    }

    protected function result(array $data): void
    {
        header('Content-Type: application/json');
        echo json_encode([
            'success' => true,
            'result' => $data
        ]);
    }
}