<?php


namespace Cristalix\Engine\Extensions;


trait FormatVerificationExtension
{
    protected function isUsernameCorrect(string $username): bool
    {
        return preg_match('/(?:^([a-zA-Z0-9_]{4,16})$|^([а-яА-Я0-9_]{4,16})$)/', $username);
    }

    protected function isEmailCorrect(string $email): bool
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    protected function isPasswordCorrect(string $password): bool
    {
        return preg_match('/^(?=.*[A-ZА-Я])(?=.*[0-9])(?=.*[a-zа-я]).{8,}$/', $password);
    }
}