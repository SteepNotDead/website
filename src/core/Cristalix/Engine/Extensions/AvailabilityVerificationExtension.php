<?php


namespace Cristalix\Engine\Extensions;

use Cristalix\Engine\Database;
use Exception;

trait AvailabilityVerificationExtension
{
    protected abstract function getDatabase(): Database;

    protected function isUsernameAvailable(string $username): bool
    {
        return empty($this->getDatabase()->queryData("SELECT id FROM users WHERE LOWER(username) = LOWER(:username)", [
                ':username' => $username
            ])) && empty($this->getDatabase()->queryData("SELECT id FROM username_log WHERE LOWER(old_username) = LOWER(:username) OR LOWER(new_username) = LOWER(:username)", [
                ':username' => $username
            ])) && empty($this->getDatabase()->queryData("SELECT id FROM registrations WHERE LOWER(username) = LOWER(:username) AND expires > to_timestamp(:time)", [
                ':username' => $username,
                ':time' => time()
            ]));
    }

    protected function isEmailAvailable(string $email): bool
    {
        return empty($this->getDatabase()->queryData("SELECT user_id FROM credentials WHERE email = :email", [
                ':email' => $email
            ])) && empty($this->getDatabase()->queryData("SELECT id FROM registrations WHERE email = :email AND expires > to_timestamp(:time)", [
                ':email' => $email,
                ':time' => time()
            ]));
    }

    protected function doesMojangUsernameExist(string $mojang_username): bool
    {
        try {
            $data_string = file_get_contents("https://api.mojang.com/users/profiles/minecraft/$mojang_username");
            if ($data_string == '') {
                return false;
            }
            $data = json_decode($data_string);
            return isset($data->id);
        } catch (Exception $e) {
            error_log($e);
            return false;
        }
    }
}