<?php


namespace Cristalix\Engine\Extensions;


use Cristalix\Model\User;

trait SessionExtension
{
    private bool $session_ready = false;

    public function initializeSessions(string $sessions_redis_url): void
    {
        ini_set('session.save_handler', 'redis');
        ini_set('session.save_path', $sessions_redis_url);
    }

    private function openSession(bool $create): void
    {
        if (!$this->session_ready) {
            if ($create || isset($_COOKIE['PHPSESSID'])) {
                session_start([
                    'cookie_lifetime' => 2592000,
                ]);
            }
            $this->session_ready = true;
        }
    }

    protected function getUser(): ?User
    {
        $this->openSession(false);

        global $_SESSION;

        return isset($_SESSION['user']) ? $_SESSION['user'] : null;
    }

    protected function setUser(?User $user)
    {
        $this->openSession(true);

        global $_SESSION;

        $_SESSION['user'] = $user;
    }
}