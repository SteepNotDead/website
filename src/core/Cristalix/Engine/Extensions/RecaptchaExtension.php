<?php


namespace Cristalix\Engine\Extensions;

use Exception;

trait RecaptchaExtension
{
    private string $recaptcha_secret;

    protected function initializeRecaptcha(string $secret): void {
        $this->recaptcha_secret = $secret;
    }

    public function checkCaptcha(string $response, string $ip, string $action): bool
    {
        $data = [
            'secret' => $this->recaptcha_secret,
            'response' => $response,
            'remoteip' => $ip
        ];

        $options = [
            'http' => [
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data)
            ]
        ];

        try {
            $result = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify',
                false, stream_context_create($options)));

            return $result->success && $result->score >= 0.5 && $result->action === $action;
        } catch (Exception $e) {
            return false;
        }
    }
}