<?php


namespace Cristalix\Engine\Extensions;

use RuntimeException;

trait TowerExtension
{
    private string $tower_rest_api_url;

    public function initializeTower(string $tower_rest_api_url): void
    {
        $this->tower_rest_api_url = $tower_rest_api_url;
    }

    public function sendPackage(string $class_name, mixed $data): void
    {
        $payload = [
            'className' => $class_name,
            'data' => $data
        ];

        $context = stream_context_create([
            'http' => [
                'method' => 'POST',
                'header' => [
                    'Content-Type: application/json'
                ],
                'content' => json_encode($payload)
            ]
        ]);

        $response = @file_get_contents($this->tower_rest_api_url . '/api/send', false, $context);

        if (!$response) {
            throw new RuntimeException('Failed to fetch Tower REST API: ' . print_r(error_get_last(), true));
        }

        $response = json_decode($response);

        if (!$response->success) {
            throw new RuntimeException('Error fetching Tower REST API: ' . $response->error);
        }
    }

    public function requestPackage(string $class_name, object $data, ?int $timeout, ?string $response_class_name): object
    {
        $payload = [
            'className' => $class_name,
            'responseClassName' => $response_class_name,
            'timeout' => $timeout,
            'data' => $data
        ];

        $context = stream_context_create([
            'http' => [
                'method' => 'POST',
                'header' => [
                    'Content-Type: application/json'
                ],
                'content' => json_encode($payload)
            ]
        ]);

        $response = @file_get_contents($this->tower_rest_api_url . '/api/request', false, $context);

        if (!$response) {
            throw new RuntimeException('Failed to fetch Tower REST API: ' . print_r(error_get_last(), true));
        }

        $response = json_decode($response);

        if (!$response->success) {
            throw new RuntimeException('Error fetching Tower REST API: ' . $response->error);
        }

        return $response->data->data;
    }
}