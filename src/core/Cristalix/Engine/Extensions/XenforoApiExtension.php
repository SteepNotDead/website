<?php


namespace Cristalix\Engine\Extensions;


use RuntimeException;

trait XenforoApiExtension
{
    private string $xenforo_api_url;
    private string $xenforo_api_key;

    public function initializeXenforoApi(array $config): void
    {
        $this->xenforo_api_url = $config['url'];
        $this->xenforo_api_key = $config['key'];
    }

    public function createForumUser(string $username, string $email): object
    {
        return $this->xenforoApiRequest('POST', 'users/', [
            'username' => $username,
            'email' => $email,
            'password' => bin2hex(random_bytes(16))
        ]);
    }

    public function getForumUser(int $user_id): ?object
    {
        return $this->xenforoApiRequest('GET', 'users/' . $user_id);
    }

    public function changeForumUserEmail(int $user_id, string $new_email): void
    {
        $this->xenforoApiRequest('POST', 'users/' . $user_id, [
            'email' => $new_email
        ]);
    }

    public function changeForumUserUsername(int $user_id, string $new_username): void
    {
        $this->xenforoApiRequest('POST', 'users/' . $user_id, [
            'username' => $new_username
        ]);
    }

    public function changeForumUserMainGroup(int $user_id, int $new_group_id): void
    {
        $this->xenforoApiRequest('POST', 'users/' . $user_id, [
            'user_group_id' => $new_group_id
        ]);
    }

    public function changeForumUserSecondaryGroups(int $user_id, array $new_secondary_group_ids): void
    {
        $this->xenforoApiRequest('POST', 'users/' . $user_id, [
            'secondary_group_ids' => $new_secondary_group_ids
        ]);
    }

    private function xenforoApiRequest(string $method, string $endpoint, array $params = []): object
    {
        $params['api_bypass_permissions'] = 1;
        $context = stream_context_create([
            'http' => [
                'method' => $method,
                'header' => [
                    'XF-Api-Key: ' . $this->xenforo_api_key,
                    'Content-Type: application/x-www-form-urlencoded'
                ],
                'content' => http_build_query($params)
            ]
        ]);

        $response = @file_get_contents($this->xenforo_api_url . $endpoint, false, $context);

        if (!$response) {
            throw new RuntimeException('Failed to fetch XenForo API: ' . print_r(error_get_last(), true));
        }

        $response = json_decode($response);

        if (isset($response->errors)) {
            throw new RuntimeException(print_r($response->errors, true));
        }

        return $response;
    }
}