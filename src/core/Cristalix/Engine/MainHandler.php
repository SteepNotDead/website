<?php

namespace Cristalix\Engine;

use Twig\Loader\FilesystemLoader;
use Twig\Environment;

if (!defined('ENGINE')) {
    define('ENGINE', 'Cristalix');
    define('__DOCUMENT_ROOT__', $_SERVER["DOCUMENT_ROOT"]);
    define('ENGINE_PATH', __DOCUMENT_ROOT__ . '/engine/');
    define('TWIG_CACHE_PATH', __DOCUMENT_ROOT__ . '/twig-cache/');
    define('TEMPLATES_PATH', __DOCUMENT_ROOT__ . '/templates/');
    define('BASE_PAGES_DIR', __DOCUMENT_ROOT__ . '/pages/');
} else {
    die('Can\'t reimport MainHandler');
}

class MainHandler
{
    private array $config;

    public function __construct()
    {
    }

    public function initialize(array $config): void
    {
        $this->config = $config;
    }

    public function handle(): void
    {
        $request = Request::createFromGlobalVars();

        $path_parts = $request->getPathParts();
        while (!empty($path_parts)) {
            $class_path_parts = $path_parts;
            foreach ($class_path_parts as &$part) {
                $part = str_replace('_', '', ucwords($part, '_'));
            }
            if (file_exists(BASE_PAGES_DIR . join('/', $class_path_parts) . 'Controller.php')) {
                /** @noinspection PhpIncludeInspection */
                include_once BASE_PAGES_DIR . join('/', $class_path_parts) . 'Controller.php';
                $controller_class = '\\Pages\\' . join('\\', $class_path_parts) . 'Controller';
                $controller = new $controller_class();
                break;
            }
            array_pop($path_parts);
        }

        if (!isset($controller)) {
            die('No controller is found');
        }

        $controller->initialize($this->config);

        $context = new RequestContext($request, MainHandler::createTwig());
        $context->getTwig()->addGlobal('path', $context->getRequest()->getPathParts());

        $controller->processRequest($context);
    }

    private static function createTwig(): Environment
    {
        $loader = new FilesystemLoader(TEMPLATES_PATH);
        return new Environment($loader, [
            'cache' => TWIG_CACHE_PATH,
            'auto_reload' => true
        ]);
    }
}