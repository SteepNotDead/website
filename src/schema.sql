create extension if not exists "uuid-ossp";

create table if not exists registrations
(
    id              bigserial                           not null
        constraint registrations_pkey
            primary key,
    username        varchar(64)                         not null,
    password        varchar(128)                        not null,
    email           varchar(128)                        not null,
    gender          integer                             not null,
    mojang_username varchar(64),
    key             varchar(16)                         not null,
    expires         timestamp default CURRENT_TIMESTAMP not null
);

create index if not exists site_registrations_expires_index
    on registrations (expires);

create index if not exists site_registrations_key_index
    on registrations (key);

create index if not exists site_registrations_username_index
    on registrations (username);

create table if not exists users
(
    id             bigserial                          not null
        constraint users_pkey
            primary key,
    forum_user_id  bigint                             not null,
    username       varchar(128)                       not null
        constraint users_username_index
            unique,
    gender         integer                            not null,
    donate_group   integer default 0                  not null,
    staff_group    integer default 0                  not null,
    color          integer default 0,
    first_username varchar(32)                        not null,
    uuid           uuid    default uuid_generate_v4() not null
);

create index if not exists users_first_username_index
    on users (first_username);

create index if not exists users_uuid_index
    on users (uuid);

create table if not exists admin_log
(
    id             bigserial                           not null
        constraint admin_log_pkey
            primary key,
    user_id        bigint                              not null
        constraint site_admin_log_users_id_fk
            references users,
    target_user_id bigint
        constraint site_admin_log_users_id_fk_2
            references users,
    timestamp      timestamp default CURRENT_TIMESTAMP not null,
    event          varchar(256)                        not null
);

create table if not exists balance_log
(
    id        bigserial    not null
        constraint balance_log_pkey
            primary key,
    user_id   bigint       not null
        constraint balance_log_users_id_fk
            references users,
    timestamp timestamp    not null,
    amount    bigint       not null,
    realm     varchar(255) not null,
    comment   varchar(255) not null
);

create index if not exists balance_log_realm_index
    on balance_log (realm);

create table if not exists balances
(
    user_id    bigint           not null
        constraint balances_pkey
            primary key
        constraint balances_users_id_fk
            references users,
    gold       bigint default 0 not null,
    experience bigint default 0 not null,
    bonuses    bigint default 0 not null,
    total_gold bigint default 0 not null
);

create table if not exists credentials
(
    user_id       bigint       not null
        constraint credentials_pkey
            primary key
        constraint user_credentials_users_id_fk
            references users,
    email         varchar(128) not null
        constraint user_credentials_email_index
            unique,
    password_hash varchar(255) not null,
    ga_secret     varchar(16)
);

create table if not exists ga_secrets
(
    user_id bigint                                    not null
        constraint ga_secrets_pkey
            primary key
        constraint user_ga_secrets_users_id_fk
            references users,
    secret  varchar(16) default ''::character varying not null
);

create table if not exists hd_subscriptions
(
    user_id bigint                not null
        constraint hd_subscriptions_pkey
            primary key
        constraint user_hd_users_id_fk
            references users,
    expires timestamp default to_timestamp(0) not null,
    auto    boolean default false not null,
    period  integer default 0     not null
);

create table if not exists mail_changes
(
    id      bigserial                           not null
        constraint mail_changes_pkey
            primary key,
    user_id bigint                              not null
        constraint site_change_mail_users_id_fk
            references users,
    email   varchar(128)                        not null,
    key     varchar(16)                         not null,
    expires timestamp default CURRENT_TIMESTAMP not null
);

create index if not exists site_change_mail_key_index
    on mail_changes (key);

create table if not exists password_recoveries
(
    id      bigserial                           not null
        constraint password_recoveries_pkey
            primary key,
    user_id bigint                              not null
        constraint site_password_recovery_users_id_fk
            references users,
    key     varchar(16)                         not null,
    expires timestamp default CURRENT_TIMESTAMP not null
);

create table if not exists payments
(
    id                 bigserial                           not null
        constraint payments_pkey
            primary key,
    user_id            bigint                              not null
        constraint site_payments_users_id_fk
            references users,
    gateway_payment_id varchar(128),
    amount             integer                             not null,
    created_on         timestamp default CURRENT_TIMESTAMP not null,
    paid_on            timestamp
);

create table if not exists username_log
(
    id           bigserial                           not null
        constraint username_log_pkey
            primary key,
    user_id      bigint                              not null
        constraint user_username_log_users_id_fk
            references users,
    timestamp     timestamp default CURRENT_TIMESTAMP not null,
    old_username varchar(32)                         not null,
    new_username varchar(32)                         not null
);

create index if not exists user_username_log_old_index
    on username_log (old_username);